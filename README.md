# PLANTBAY - Computer Science IA
<img src="https://raw.githubusercontent.com/tanujdargan/plantbay/main/plantbay.png?token=GHSAT0AAAAAABSBHTQNIGKE6GMYIA27JW7KYR7HLUQ" align="center"/>

# Firebase Setup
- Step 1: Create a new project
- Step 2: Register a new app under web and copy the configuration key
- Step 3: Click continue to console and begin setting up authentication
- Step 4: Setup email and password + google authentication options
- Step 5: Setup Realtime database with closest server location and start in test mode
- Step 6: Enable storage private

## Virtual Environment to develop in
```
pip install virtualenv
```

```
virtualenv plantbay
```

```
virtual .\plantbay\Scripts\activate
```
